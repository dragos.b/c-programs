#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <time.h>

void
banner ( const char *titulo ) {
    char comando[200] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}
int
main (int argc, char *argv[]) {
    banner ( "Game Life" );
    struct winsize wsize;
    int row,
        col,
        *cel,
        *comput,
        board_size,
        random;
    time_t seconds;

    // Guadamos en row las flias, en col las columnas accediendo
    // al struct wsize.
    ioctl ( STDOUT_FILENO, TIOCGWINSZ, &wsize);

    row = ( wsize.ws_row - 6 );
    col = wsize.ws_col;

    // El tamaño del tablero
    board_size = row * col;

    // Reserva dinamica de las celulas y el computo
    // del tamaño del tablero para alojar enteros.
    cel = (int *) malloc ( board_size * sizeof(int) );
    comput = (int *) malloc ( board_size * sizeof(int) );

    // Rellena el tablero de celulas y computo a 0
    bzero ( cel, board_size * sizeof(int) );
    bzero ( comput, board_size * sizeof(int) );

    // Segundos hora local
    seconds = time (&seconds) % 60;

    // Generar Aleatorio en función del modulo de los segundos y el tamaño del tablero
    srand(seconds);
    random = rand() % board_size;

    printf("Random:%i\nBoardSize:%i\n", random, board_size );

    // Enviar valor aleatorio a un función y que en esa posición guarde un 1

    // Imprime
    for (int i=0; i<board_size; i++)
        printf("%i", cel[i] );
    printf("\n");



    free(cel);
    free(comput);

    return EXIT_SUCCESS;
}
