#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define TOT 10
void
banner ( const char *titulo ) {
    char comando[200] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}
int
main (int argc, char *argv[]) {
    banner ( "Caracteres ASCII" );
    char texto[TOT];
    for ( int i=0; i<TOT; i++ )
        scanf ( " %c", &texto[i] );

    for ( int i=0 ; i<TOT; i++ )
        printf ( "%c -- %i\n", texto[i], texto[i] );
    return EXIT_SUCCESS;
}
