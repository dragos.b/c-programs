#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

int
main (int argc, char *argv[]) {
    banner ( "05" );
    char *caracteres = NULL, buff;
    int s = 0;
    // Entrada
    printf ( "\tIntroducir caracteres\n\t    Finaliza con *\n" );
    do {
        scanf ( " %c", &buff );
        caracteres = (char*) realloc ( caracteres, (s+1) * sizeof(char) );
        *( caracteres + s ) = buff;
        s++;
    } while ( buff != '*' );
    caracteres = (char*) realloc ( caracteres, (s-1) * sizeof(char) );
    s -= 1;

    // Salida
    banner ( "05" );

    for (int i=0; i<s; i++)
        printf(" %c ", *( caracteres+i ) );
    printf( "\n" );


    for (int i=0; i<s; i++)
        printf("%i ", *( caracteres+i ) );
    printf( "\n" );

    free(caracteres);

    return EXIT_SUCCESS;
}
