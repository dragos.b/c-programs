#ifndef __CPERSONA_H__
#define __CPERSONA_H__

#define M 0x100
class CPersona {

    char     nombre   [M];
    char     apellido [M];
    unsigned edad;

    public:

    // Constructores
    CPersona () = delete;   // Elimina el constructor por defecto.
    CPersona ( const char *nom, unsigned ed );
    CPersona ( const char *nom, const char *ape, unsigned ed );

    // Metodos
    unsigned getEdad     ();
    char*    getNombre   ();
    char*    getApellido ();

};

#endif
