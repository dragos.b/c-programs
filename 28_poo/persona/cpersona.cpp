#include <string.h>
#include <stdio.h>
#include "cpersona.h"

CPersona::CPersona ( const char *nom, const char *ape, unsigned ed ) : edad(ed)
{
    strcpy ( this->nombre,   nom );
    strcpy ( this->apellido, ape );
}

CPersona::CPersona ( const char *nom, unsigned ed ) : edad(ed)
{
    strcpy ( this->nombre, nom );
    strcpy ( this->apellido, "Expósito" );
}

unsigned
CPersona::getEdad()
{
    return this->edad;
}

char*
CPersona::getNombre()
{
    return this->nombre;
}

char*
CPersona::getApellido()
{
    return this->apellido;
}



