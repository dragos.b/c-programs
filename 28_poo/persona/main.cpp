#include <stdio.h>
#include <stdlib.h>
#include "cpersona.h"

#define MAX_P 0xA

void
banner () {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t '< < < PERSONAS > > >'";
    system ( command );
}

void
imprime_personas ( CPersona *personas ) {

    for ( int i=0; i<MAX_P; i++ ) {
        printf ( "\n\t%i.- Hola %s %s, tienes %u años !!\n\n",
                i+1,
                personas[i].getNombre   (),
                personas[i].getApellido (),
                personas[i].getEdad     () );
    }
}

int
main ( int argc, char *argv[] ) {

    banner ();
    const char *nombre[] = {
        "Juan",
        "Benito",
        "Almudena",
        "Eustaquio",
        "Ramón",
        "Alicia",
        "Fulgencio",
        "Sisebuto",
        "Raquel",
        "Marta",
        NULL
    };

    CPersona *personas = NULL;                                      // Puntero a clase CPersona.
    personas = (CPersona*) malloc ( sizeof(CPersona) * MAX_P );     // Reservo espacio para 10 CPersona.

    for ( int i=0; i<MAX_P; i++ )
        personas[i] = CPersona ( nombre[i], 23 );

    imprime_personas ( personas );


    return EXIT_SUCCESS;
}
