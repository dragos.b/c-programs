#include "cpersona.h"
#include <string.h>
#include <stdio.h>

// Constructor, como no tiene que devolver nada no se pone.
CPersona::CPersona ( const char *n, const char *ap, unsigned ed )
{
    strcpy ( this->nombre,   n  );
    strcpy ( this->apellido, ap );
    this->edad = ed;
}

void
CPersona::display () {
    printf ( "Nombre: %s\nApellido: %s\nEdad: %i\n",
            this->nombre,
            this->apellido,
            this->edad  );
}

void
CPersona::setNombre ( const char *n ) {
    strcpy ( this->nombre, n );
}

const char*
CPersona::getNombre () {
    return this->nombre;
}
