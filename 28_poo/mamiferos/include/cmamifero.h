#ifndef __CMAMIFERO_H__
#define __CMAMIFERO_H__

enum class TipoPelo
{ Completo, Medio, Escaso };

class CMamifero
{

  unsigned n_extremidades;
  TipoPelo tipoPelo;
  double temperatura;  // Como Fahrenheit

protected:			   // protected se usa para el uso en clases derivadas.

    CMamifero () = delete;
    CMamifero (unsigned extremidades, double temp, TipoPelo pelo = TipoPelo::Completo);
    CMamifero (unsigned extremidades, TipoPelo pelo = TipoPelo::Completo);
  //                                                  ^                ^
  //              Variable del tipo TPelo. Por defecto es TPelo::Completo
  //              Escribimos TPelo::Completo ya que si ponemos Completo no
  //              sabe de quien es.

  // Destructor, nunca llamar de forma manual.
   ~CMamifero ();

public:
    virtual void anda () = 0;
    virtual void anda (double x, double y) = 0;

    double getTempFah ();   // Fahrenheit
    double getTempCel ();   // Celsius
    double getTempKel ();   // Kelvin


};

#endif
