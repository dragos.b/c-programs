#include <string.h>
#include <stdio.h>
#include "chumano.h"
#include "cmamifero.h"

CHumano::CHumano (unsigned extrem, const char *ojos, TipoPelo pelo):
CMamifero (extrem, pelo)
{
  strcpy (this->colorOjos, ojos);
}

CHumano::CHumano (unsigned extrem, double temp, const char *ojos,
		  TipoPelo pelo):
CMamifero (extrem, temp, pelo)
{
  strcpy (this->colorOjos, ojos);
}

void
CHumano::anda ()
{
  printf ("Moviendo al Humano sin posición\n");
}

void
CHumano::anda (double x, double y)
{
  printf ("El Humano se mueve a la posición:\n\tx: %5.1lf\n\ty: %5.1lf\n\n",
	  x, y);
}
