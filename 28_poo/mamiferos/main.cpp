#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "chumano.h"
#include "ccaballo.h"
#include "cmamifero.h"

void
banner ()
{
  char command[0x100] =
    "clear ; toilet -fpagga -F border:border:crop -t '< < < MAMIFEROS > > >'";
  system (command);
}

int
main (int argc, char *argv[])
{
  banner ();

  // Instancia de los objetos
  CCaballo caballo  = CCaballo ("000", true, 4, TipoPelo::Medio);
  CCaballo caballo2 = CCaballo ("C31", false, 4);
  CCaballo caballo3 = CCaballo ("FFF", true, 4, 132.32, TipoPelo::Completo);
  CHumano humano = CHumano (4, "B70");

  // Ejecucion de los metodos de cada objeto o un metodo en comun virutal.
  caballo.relincha  ();
  caballo2.relincha ();

  humano.anda   (2, 4);
  humano.anda   ();
  caballo2.anda ();

  caballo.anda  (44.3, 34.66);
  caballo3.anda (44.3, 34.66);

  printf ( "Caballo 3 Fº: %.2lf\n", caballo3.getTempFah() );
  printf ( "Caballo 3 Cº: %.2lf\n", caballo3.getTempCel() );
  printf ( "Caballo 3 Kº: %.2lf\n", caballo3.getTempKel() );

  return EXIT_SUCCESS;
}
