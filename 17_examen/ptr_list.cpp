#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    banner ( "ptr lista" );
    const char *lista[] = {
        "Juan",
        "Alex",
        "Jose"
    };


    printf ("%p\n", lista);
    printf ("%p\n", &lista);

    printf ("%p\n", *lista);
    printf ("%p\n", *(lista+1));
    printf ("%p\n", *(lista+2));


    printf ("%c\n", **lista);
    printf ("%c\n", *(*(lista)+1));
    printf ("%c\n", **lista);


    return EXIT_SUCCESS;
}
