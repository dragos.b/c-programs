#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    banner ( "ptr size" );
    int *ptr_i;
    char *ptr_c;
    double *ptr_d;

    printf ( "Tamaño ptr_i: %lu \n", sizeof(ptr_i) );
    printf ( "Tamaño ptr_c: %lu \n", sizeof(ptr_c) );
    printf ( "Tamaño ptr_d: %lu \n", sizeof(ptr_d) );


    return EXIT_SUCCESS;
}
