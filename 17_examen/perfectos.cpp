#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 5
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    banner ( "N. Perfecto" );
    int num=2, res;

    for (int i=0;i<MAX;num++){
        res=0;
        for (int j=1;j<num;j++)
            if ( (num%j) == 0 )
                res+=j;
        if (num == res) {
            i++;
            printf("%i\n", num);
        }
    }

    return EXIT_SUCCESS;
}
