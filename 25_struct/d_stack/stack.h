#ifndef __STACK_H__
#define __STACK_H__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct TStack {
    int summit;
    int *data;
    unsigned size;
    int fail;
};

#ifdef __cplusplus
extern "C" {
#endif

void allocate ( struct TStack *st );
void destroy  ( const struct TStack *st );
void init     ( struct TStack *st );
void push     ( struct TStack *st, int new_data );
int peek      ( struct TStack *st );
void pop      ( struct TStack *st );

#ifdef __cplusplus
}
#endif

#endif
