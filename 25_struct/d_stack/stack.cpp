#include "stack.h"
#define BLCSZ 5

// Trabajar con pilas pero sin llegar a tocarla a mano.
void
allocate ( struct TStack *st ) {
    st->data = (int *) realloc ( st->data, ( BLCSZ + st->size ) * sizeof(int) );
    st->size += BLCSZ;
}

void
init ( struct TStack *st ) {
    st->data   = NULL;
    st->size   = 0;
    st->fail   = 0;
    st->summit = 0;
    allocate (st);
}

void
push ( struct TStack *st, int new_data ) {
    if ( st->summit >= st->size )
        allocate ( st );

    st->data[st->summit++] = new_data;
}

int
peek ( struct TStack *st ) { return st->data[st->summit - 1]; }

void
pop ( struct TStack *st ) {
    st->fail = 0;
    if ( st->summit > 0 ) {
        st->data[--st->summit];
        --st->size;
    }
    else st->fail = 1;
}

void
destroy ( const struct TStack *st ) {
    free( st->data );
}
