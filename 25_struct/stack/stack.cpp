#include "stack.h"

void
init ( struct TStack *st ) {
    st->fail   = 0;
    st->summit = 0;
}

void
push ( struct TStack *st, int new_data ) {
    st->data[st->summit++] = new_data;
}

int
peek ( struct TStack *st ) {
    return st->data[st->summit - 1];

}

void
pop ( struct TStack *st ) {
    st->fail = 0;
    if ( st->summit > 0 )
        st->data[--st->summit];
    else st->fail = 1;
}

