#ifndef __STACK_H__
#define __STACK_H__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define M 5

struct TStack {
    int summit;
    int data[M];
    int fail;
};

#ifdef __cplusplus
extern "C" {
#endif

void init ( struct TStack *st );
void push ( struct TStack *st, int new_data );
int peek  ( struct TStack *st );
void pop  ( struct TStack *st );

#ifdef __cplusplus
}
#endif

#endif
