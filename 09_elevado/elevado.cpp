#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main (int argc, char *argv[]) {

    int base, elevado;
    /**
     * Scanf number base and number to elevate base
     */
    printf ("Número base: ");
    scanf (" %i", &base);
    printf ("%i elevado n veces: ", base);
    scanf (" %i", &elevado);

    /**
     * For loop to calculate n^x, n^x+1, n^x+2...
     * In for use pow function, this function is defined in math.h 
     */

    for ( int i = 0; i <= elevado; i++  )
        printf("%i^%i = %.0f\n",base, i, pow (base, i) );

    return EXIT_SUCCESS;
}
