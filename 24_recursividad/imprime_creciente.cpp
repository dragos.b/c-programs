#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
banner ( const char *titulo ) {
    char comando[250] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

unsigned
imprime ( unsigned ini, unsigned max ) {
    if ( ini > max )
        return ini;
    printf ( "%i\n", ini);
    imprime ( ini + 1, max );
}

int
main (int argc, char *argv[]) {
    banner ( "Imprime Creciente" );

    // Ejecutamos de la siguiente manera:
    // $: imprime_n 1 12
    // 1 es el inicio
    // 12 el maximo

    unsigned ini = atoi ( argv[1] ),
             max = atoi ( argv[2] );

    imprime ( ini, max );

    return EXIT_SUCCESS;
}
