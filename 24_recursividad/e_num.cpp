#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#define TITLE banner ( " ' < < <  Numero e  > > > ' " )

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

double
factorial ( int num ) {
    if ( num == 1 )       // Calcula num!, el factorial de num.
        return 1;
    return num * factorial ( num - 1 );
}

double
e ( double max ) {
    if ( max == 0 )       // e(3) = 1/3! + 1/2! + 1/1! + 1/0!
        return 1;
    return 1 / factorial(max) + e(max - 1);
}

void
use () {
    char *user =  getenv ( "USER" );
    char hostn[255];
    gethostname ( hostn, sizeof(hostn) );
    printf ( "Error al ejecutar\n\
            \rUsar como:\n\
            \r%s@%s $: ./e_num N\n\n\
            \rDonde N > 0\n\n", user, hostn );
}

int
main ( int argc, char *argv[] ) {
    double num, res;
    bool param;

    TITLE;
    param = argc > 1 ? true : false;   // Verifica si al ejecutar hemos pasado numero como parametro

    // Ejecuta si hemos pasado numero como parametro.
    if ( param ) {
        num = atoi ( argv[1] );

        printf ( "\t\tCalculando...\n" );
        res = e ( num );

        TITLE;
        printf ( "\te(%.0lf) es:\n\n\t%.20lf\n\n", num, res );
    }
    else use();

    return EXIT_SUCCESS;
}
