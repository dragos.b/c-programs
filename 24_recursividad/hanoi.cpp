#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 5

enum TPalo { Origen, Intermedio, Destino };

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

void
pasar_discos (  int *origen, int *intermedio, int *destino ) {
    int static n = N;

    if ( n == 0 )
        return;

    if ( n > 0 ) {
        for ( int i=0; i<N; i++)
            intermedio[i] = origen[i];
        n--;
        pasar_discos ( intermedio, destino, origen );


    }

}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Torres Hanoi  > > > ' " );

    int palo[3][N] = {
        { 5, 4, 3, 2, 1 }
    },
        cima[3];
    int *origen     = &palo [ Origen ]     [ Origen ];
    int *intermedio = &palo [ Intermedio ] [ Origen ];
    int *destino    = &palo [ Destino ]    [ Origen ];


    pasar_discos ( origen, intermedio, destino );



    return EXIT_SUCCESS;
}
