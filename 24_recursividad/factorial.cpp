#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

unsigned askn ( unsigned num ) {
    printf ( "\tNumero:\n\t" );
    scanf ( "%u", &num );
    int es = isalnum ( num );

    if (  es = 0 )
        printf ( "No es numero\n" );
    else return num;

}

unsigned factorial ( unsigned num ) {

    if ( num == 1)
        return 1;

    return num * factorial ( num - 1 );

}

int
main (int argc, char *argv[]) {
    banner ( "Factorial" );
    unsigned num, res;

    num = askn ( num );
    res = factorial ( num );
    printf ( "Factorial de %u = %u\n", num, res );

    return EXIT_SUCCESS;
}

