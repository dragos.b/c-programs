#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern char **environ;
void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

int
main ( int argc, char *argv[] ) {
    banner ( " ' < < <  Skel  > > > ' " );
    for (char **env = environ; *env; ++env)
        printf("%s\n", *env);

    return EXIT_SUCCESS;
}
