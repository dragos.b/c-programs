#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define BL      printf   ( "\n" )
#define IS(X)   printf   ( "%i ¡ PRIMO !", X )
#define ISNT(X) printf   ( "%i NO PRIMO :(", X )
#define DIVS(N) divisors ( N, 1 )

void
banner ( const char *title ) {
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}

int
divisors ( int num, int ini ) {
    static int fin = 0;
    if ( ini > (num / 2) )
        return fin;

    if ( num % ini == 0 )
        fin++;

        divisors ( num, ini + 1 );
}

bool
cousin ( int n ) { return DIVS(n) == true ? true : false; }

int
main ( int argc, char *argv[] ) {
    banner ( " ' < <  Es Primo ?  > > ' " );
    int num = atoi ( argv[1] );

    cousin(num) ? IS(num) : ISNT(num);
    BL;

    return EXIT_SUCCESS;
}
