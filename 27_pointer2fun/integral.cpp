#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

const char *limits[] = { "Limite Inferior", "Limite Superior", "Avance" };
const char *func[] = { "2x²", "x²", "2x² * 4" };
enum TLimit { li, ls, av, TOT };

void
banner ( void ) {
    const char *title = "' < < < Integral > > > '";
    char command[128] = "clear ; toilet -fpagga -F border:border:crop -t ";
    system ( strcat( command, title ) );
}
double pol_1 (double li, double ls, double av ) {   // 2x²
    if ( li > ls )
        return 0;
    return 2 * pow(li,2) + pol_1 (li + av, ls, av);
}
double pol_2 (double li, double ls, double av ) {   // x²
    if ( li > ls )
        return 0;
    return pow(li, 2) + pol_2 (li + av, ls, av);
}

double pol_3 (double li, double ls, double av ) {   // 2x² * 4
    if ( li > ls )
        return 0;
    return 2 * pow (li,2) * 4 + pol_3 (li + av, ls, av);
}
void
scan_values ( double *values ) {
    banner ();
    for ( int i=0; i<TOT; i++ ) {
        printf ( "%s\n", limits[i] );
        scanf ( "%lf", &values[i] );
    }
}
int
ask_pol ( void ) {
    banner ();
    int opc;
    printf ( "Selecciona Función\n" );
    for ( int i=0; i<TOT; i++ )
        printf ( "\t%i.- %s\n", i+1, func[i] );
    scanf ( "%i", &opc );
    printf ( "\n" );
    return opc - 1;
}

int
main ( int argc, char *argv[] ) {
    int opc;
    double values[TOT],
           ( *fun[TOT] ) ( double, double, double ) = {
               &pol_1,
               &pol_2,
               &pol_3
           },
           area;

    opc = ask_pol();                                                         // Selecionar la función (1, 2 o 3)
    scan_values ( values );                                                  // Escanea li, ls, av que se guarda en un array.
    area = (*fun[opc]) (values[li], values[ls], values[av]) * values[av];    // Guarda el valor de retorno de la función seleccionada

    printf ( "El area de\n\t%s\n\tcon li=%.1lf, ls=%.1lf y av=%.1lf\n\tEs: %.2lf\n",
            func[opc],
            values[li],
            values[ls],
            values[av],
            area );

    return EXIT_SUCCESS;
}
