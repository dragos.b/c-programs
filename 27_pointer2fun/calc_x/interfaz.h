#ifndef __INTERFAZ_H__
#define __INTERFAZ_H__

extern const char *funciones[];

#ifdef __cplusplus
extern "C"
{
#endif

void banner (const char *title);
void print_opt (void);
void print_err (void);

#ifdef __cplusplus
}
#endif

#endif
