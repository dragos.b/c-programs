#include <math.h>

double
fact ( double num) {
    if ( num == 1 )
        return 1;
    return num * fact (num-1);
}

double fun_1 ( double x ) { return x * pow (2,2);        } // X * 2²
double fun_2 ( double x ) { return 2 * pow (x, 2);       } // 2 * X²
double fun_3 ( double x ) { return pow (x, 2) * fact(x); } // X² * X!
double fun_4 ( double x ) { return (x * 10) / pow (x,2); } // (X * 10) / X²
