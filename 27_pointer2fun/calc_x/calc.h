#ifndef __CALC_H__
#define __CALC_H__

#ifdef __cplusplus
extern "C" {
#endif

double fun_1 (double x);
double fun_2 (double x);
double fun_3 (double x);
double fun_4 (double x);

#ifdef __cplusplus
}
#endif

#endif
