# Programs in C that I do in class
---
## The extension for programs is .cpp but we program in C not in C++ because C++ permites create variables in loops definition.


# Standard to write documentation in C-Programs

```cpp
/**
* @brief Brief description to function
* @details More details
*
* @return void
* @param phrase example text description for param
* @var i example text description for var i
*/


void print(const char *phrase) {
    for (int i=0; i<strlen(phrase); i++)
        printf ( "%c\n", phrase[i] );
}

```
# Separacion del programa

A medida que aumentan las lineas de codigo el programa en unico archivo es menos legible,
  debemos intentar dejar el `main` lo mas corto posible, y de un vistazo leerlo como un
  texto.

## Simulacion

  Tenemos el programa multiplicacion, que en el introducimos datos y se multiplican.

  Vamos a tener el archivo `main.cpp` en el que tenemos por separado la `Entrada` de datos,
  los `Calculos` y la `Salida`
  ```cpp

    #include <stdio.h>

    int main () {
        int num_1, num_2, res;
        // Entrada

        input (&num_1, &num_1);

        // Calculos

        res = mult (num_1, num_2);

        // Salida
        printf ( "%i x %i = %i", num_1, num_2, res );
    }

  ```

  La funcion `main` no conoce las funciones `input()` y `mult()` ya que no estan declaradas ni
  definidas.


### Creacion interfaz.cpp y cabeceras.h


  Creamos el archivo `interfaz.cpp` que **guardara** la parte del programa que sea de interfaz,
  tanto de **entrada** como de **salida** de datos.

  ```cpp

    #include <stdio.h>

    void input (int *n1, int *n2) {
        // Introduces dos numeros
        printf ( "Numero 1:\n" );
        scanf ( " %i", &n1 );

        printf ( "Numero 2:\n" );
        scanf ( " %i", &n2 );

        /** A la funcion le hacemos un paso por referencia no tenemos que retornar
          * nada ya que modificamos lo que hay en una celda de la ram
          */
    }

    int mult (int n1, int n2) { return n1 * n2; }

  ```

  Tenemos el archivo `interfaz.cpp` *(no debe de tener el bloque de calculos, pero como el
          programa es corto lo dejo de esta forma)* y ahora `main.cpp` tiene que saber
  que existe `interfaz.cpp`.

  Es por ello que creamos un archivo de cabecera *(header)* llamado `interfaz.h`.

  Los archivos de cabecera **solo** pueden contener **Declaraciones de Funciones Publicas**, y
  datos comunes como `#define` y `enum`.

  Tambien podemos incluir otras librerias como las comunes `common.h` que contiene una constante
  llamada `X` que vale 2, si hacemos uso de `X` debemos incluir en `interfaz.h` a `common.h`.

  ```cpp

    #ifndef __INTERFAZ_H__          // Este if se utiliza para saber que el preprocesador no debe
    #define __INTERFAZ_H__          // introducir la declaracion de las funciones mas de una vez en
                                    // el programa.

    #ifndef __cplusplus             // Este if se utiliza para que el preprocesador sepa que
    extern "C"                      // hemos programado en C pero compilamos con g++ y que no
    {                               // modifique el nombre de las funciones, tambien agregamos
    #endif                          // el token `extern` de C, que indica que la funcion se encuentra
                                    // en otro lado.

    /* Declaracion Funciones (externas) */

    void input (int *n1, int *n2);
    int mult (int n1, int n2);

    #ifndef __cplusplus
    }
    #endif

    #endif                          // Fin del primer if

  ```

  Ahora ya tenemos las funciones de **entrada/calculos** en otro archivo, `interfaz.cpp` y la
  declaracion de las funciones en un archivo de cabecera `interfaz.h`, con lo cual debemos
  incluir en `main.cpp` nuestro archivo de cabecera.

  ```mermaid
  graph LR;
  A[Funciones Separadas] --> B[interfaz.cpp];
  C[Declaracion Funciones Externas] --> D[interfaz.h];
  E[En main.cpp incluimos] --> F[interfaz.h];

  ```

  ```cpp

    #include <stdio.h>
    #include "interfaz.h"

    int main () {
        int num_1, num_2, res;
        // Entrada

        input (&num_1, &num_1);

        // Calculos

        res = mult (num_1, num_2);

        // Salida
        printf ( "%i x %i = %i", num_1, num_2, res );
    }

  ```
  **En este caso** `interfaz.h` **solo lo incluimos en** `main.cpp` **pero si en** `interfaz.h`
  **hemos incluido otro archivo header comun por ejemplo** `common.h` **si debemos de incluir**
  `interfaz.h` **en** `interfaz.cpp`


### Compilacion y Linkado

  Cuando tenemos el programa listo debemos de compilar todos los `.cpp` a `.o` *(object)*
  y cuando tengamos compilados todos los programas a objetos debemos linkar los mismos para
  crear el programa ejecutable.

  ```mermaid
  graph TD;
  A[main.cpp] --> |compilacion| B(main.o);
  C[interfaz.cpp] --> |compilacion| D(interfaz.o);
  B[main.o] --> |linkado| E{{multiplicacion}};
  D[interfaz.o] --> |linkado| E{{multiplicacion}};

  ```

  ```shell

  # Compilacion de cada archivo uno por uno (se puede todos a la vez tambien)
  # Si queremos depurar, en cada compilacion y linkado insertamos el argumento -g

  user@host$: g++ -c interfaz.cpp
  user@host$: g++ -c main.cpp


  user@host$: g++ -o multiplicacion interfaz.o main.o


  # Compilar para depurar:

  user@host$: g++ -g -c interfaz.cpp
  user@host$: g++ -g -c main.cpp


  user@host$: g++ -g -o multiplicacion interfaz.o main.o

  ```

> Tener el programa separado en diferentes archivos es util para tenerlo todo mucho mas
> organizado/limpio.
> Tambien es muy util porque si compilamos un programa y tarda varias horas, cuando hagamos
> cambios en diferentes funciones no debemos compilarlo de nuevo todo entero, solamente las
> partes donde hayamos hecho los cambios y a continuacion hacer el linkado.

