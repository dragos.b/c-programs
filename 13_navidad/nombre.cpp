#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TOTAL 10

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Nombre Básico";
    banner ( titulo );

    /* Finish Banner*/

    char name[] = "Dragos";

    for ( int i = 0; i<TOTAL; i++ )
        printf ("%i.- Your name is: %s\n", i+1,  name);

    return EXIT_SUCCESS;
}

