#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 25
void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Nombre Fgets";
    banner ( titulo );

    /* Finish Banner*/

    char name[MAX];
    int vez;
    printf ( "Introduce un nombre a imprimir: " );


    /* fgets save value in pointer to 'name', the size of 'name' is 'MAX' or sizeof(name), and stdin is the pointer to a FILE, in this case is Standard Input */
    fgets ( name, MAX, stdin );


    printf ( "Introduce número de veces a imprimir %s: ", name );
    scanf ( " %i", &vez );

    for ( int i = 0; i<vez; i++ )
        printf ("%i.- Your name is: %s\n", i+1,  name);

    return EXIT_SUCCESS;
}

