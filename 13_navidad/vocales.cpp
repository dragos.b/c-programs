#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Vocales";
    banner ( titulo );

    /* Finish Banner*/

    char nombre[] = "Supercalifragilisticoespialidoso";
    int a, e, i, o, u;

    a = e = i = o = u = 0;

    /* Recorremos las celdas para contar la cantidad de vocales */
    for (int j=0; nombre[j] != '\0'; j++) {
        switch (nombre[j]) {
            case 'a':
                a++;
                break;
            case 'e':
                e++;
                break;
            case 'i':
                i++;
                break;
            case 'o':
                o++;
                break;
            case 'u':
                u++;
                break;

        }

    }

    printf ( "En %s hay:\n%i 'a'\n%i 'e'\n%i 'i'\n%i 'o'\n%i 'u'\n",
            nombre,
            a,
            e,
            i,
            o,
            u );

    return EXIT_SUCCESS;
}

