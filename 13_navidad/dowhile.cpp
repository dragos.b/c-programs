#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Do While";
    banner ( titulo );

    /* Finish Banner*/

    int i = 1;
    do {
        printf ( "El valor de la variable es: %i\nIntroduce nuevo valor:\n", i );
        scanf ( " %i", &i );
    } while ( i>0 && i<11 );
    printf ( "El valor no está en el rango 1-10, vale %i\n", i );

    return EXIT_SUCCESS;
}

