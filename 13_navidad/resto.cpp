#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/
    char titulo[] = "Resto";
    banner ( titulo );
    /* Finish Banner*/

    /* Declaration of varaibles and assign values */
    int divisor = 7, resto;
    float dividendo = 13.0;

    /* In var resto save mod value of 'dividendo' % 'divisor', and convert to int
     * 'dividendo' */
    resto = (int) dividendo % divisor;

    printf ( "El resto al divdir %.1f / %i es: %i\n",
          dividendo,
          divisor,
          resto );

    return EXIT_SUCCESS;
}

