
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

const char *nombre[] = {
    "Txema",
    "Fulgencio",
    "Bonifacio",
    "Ambrosio",
    "Onorato",
    "Rogelio",
    "Anacleto",
    "Eusebio",
    "Aniceto",
    "Higino",
    NULL
};

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Nombres Favoritos";
    banner ( titulo );

    /* Finish Banner*/

    enum nombres {txema, fulgencio, bonifacio, ambrosio, onorato, rogelio, anacleto, eusebio, aniceto, higinio, FIN};
    for (int i=0; i<FIN; i++)
        printf ("%s\n", nombre[i]);

    return EXIT_SUCCESS;
}

