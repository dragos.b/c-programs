#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/

    char titulo[] = "Rectangulo";
    banner ( titulo );

    /* Finish Banner*/

    int fila, columna;
    printf ( "Introduce filas: \n" );
    scanf (" %i", &fila);
    printf ( "Introduce columnas: \n" );
    scanf ( " %i", &columna );

    for (int i=0; i<fila; i++){
        printf ("\n");
        for (int j=0; j<columna; j++)
            printf("#");
    }
    printf ("\n");
    return EXIT_SUCCESS;
}

