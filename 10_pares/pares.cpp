#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
/**
 * Calculate the even numbers in a range
 * Range is 'total' value
 */
    int total;
    printf("Total del rango para ver números pares: ");
    scanf(" %i", &total);

    printf("\nNúmeros pares comprendidos entre el 1 y %i son:\n\n", total);
    for ( int i = 1; i <= total; i++ ){
        if ( i%2 == 0 )
            printf("┋%i┋ ", i);
    }
    printf("\n");

    return EXIT_SUCCESS;
}
