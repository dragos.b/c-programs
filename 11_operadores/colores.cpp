#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Define ANSI colours
 */
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define BLUE "\x1b[34m"

#define CYAN "\x1b[36m"
#define MAGENTA "\x1b[35m"
#define YELLOW "\x1b[33m"

#define BLACK "\x1b[30m"
#define WHITE "\x1b[37m"

void banner ( char titulo[] ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {

    /*Banner title*/
    char titulo[] = "Colores";
    banner ( titulo );
    /* Finish Banner*/

    char red, green, blue;
    const char *eleccion[] = {
        "ROJO",
        "VERDE",
        "AZUL",
        "CYAN",
        "AMARILLO",
        "MAGENTA",
        "BLANCO",
        "NEGRO"
    };
    /**
     * Input block
     */
    printf ( "¿Qué colores activar?\n" );

    printf ( "%sRojo %s(s)%si o %s(n)%so: \n",
            RED,
            GREEN,
            WHITE,
            RED,
            WHITE );
    scanf ( " %c", &red );

    printf ( "%sVerde (s)%si o %s(n)%so: \n",
            GREEN,
            WHITE,
            RED,
            WHITE );
    scanf ( " %c", &green );

    printf ( "%sAzul %s(s)%si o %s(n)%so: \n",
            BLUE,
            GREEN,
            WHITE,
            RED,
            WHITE );
    scanf ( " %c", &blue );

    /**
     * Calculation block
     */
    if ( red == 's' && green == 'n' && blue == 'n' )
        printf ("\nHas elegido: %s%s\n", RED, eleccion[0] );

    if ( red == 'n' && green == 's' && blue == 'n' )
        printf ( "\nHas elegido: %s%s\n", GREEN, eleccion[1] );

    if ( red == 'n' && green == 'n' && blue == 's' )
        printf ( "\nHas elegido: %s%s\n", BLUE, eleccion[2] );

    if ( red == 'n' && green == 's' && blue == 's' )
        printf ( "\nHas elegido: %s%s\n", CYAN, eleccion[3] );

    if ( red == 's' && green == 's' && blue == 'n' )
        printf ( "\nHas elegido: %s%s\n", YELLOW, eleccion[4] );

    if ( red == 's' && green == 'n' && blue == 's' )
        printf ( "\nHas elegido: %s%s\n", MAGENTA, eleccion[5] );

    if ( red == 's' && green == 's' && blue == 's' )
        printf ( "\nHas elegido: %s%s\n", WHITE, eleccion[6] );


    if ( red == 'n' && green == 'n' && blue == 'n' )
        printf ( "\nHas elegido: %s%s\n", BLACK , eleccion[7] );

    return EXIT_SUCCESS;
}

