#ifndef __COLORS_H__
#define __COLORS_H__

// Primary
#define RED       "\x1b[31m"
#define GREEN     "\x1b[92m"
#define BLUE      "\x1b[34m"

// Secondary
#define YELLOW    "\x1b[93m"
#define MAGENTA   "\x1b[35m"
#define CYAN      "\x1b[36m"

#define BLACK     "\x1b[38;5;16m"
#define WHITE     "\x1b[38;5;255m"

// Format
#define BOLD      "\x1b[01m"
#define ITALIC    "\x1b[03m"
#define UNDERLINE "\x1b[04m"
#define CROSS     "\x1b[09m"

// Backgrounds
#define BG_RED    "\x1b[48;5;196m"
#define BG_GREEN  "\x1b[48;5;34m"
#define BG_BLUE   "\x1b[48;5;20m"

#define BG_YELLOW  "\x1b[48;5;214m"
#define BG_MAGENTA "\x1b[48;5;99m"
#define BG_CYAN    "\x1b[48;5;38m"

#define BG_BLACK   "\x1b[48;5;16m"
#define BG_WHITE   "\x1b[48;5;255m"

#define RESET      "\x1b[0m"

#endif
