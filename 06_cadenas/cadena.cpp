#include <stdio.h>

int main() {
    char cadena[] = "Es una cadena sin tamaño";
    /**
     * Print 1 char, in this case print [0] and [3]
     */
    printf("%c\n%c\n", *cadena, cadena[3]);
    /**
     * Print the string
     */
    printf("%s\n", cadena);
    printf("\nTamaño de la cadena: %lu \n", sizeof(cadena));
    return 0;
}
