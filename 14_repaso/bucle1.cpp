#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border:gay -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {

    unsigned num, buffer, res, init;

    // Entrada
    banner ( "Bucle" );
    printf ( "Introduce Número\n" );
    scanf ( " %u", &num );

    // Calculos & Salida
    /*
       for ( int i=0 ; i<num; i++, buffer--) {
       res = num % buffer; 
       if ( res == 0)
       printf ( "%u %% %u = 0\n", num, buffer );
       }
       */

    init = num;
    for (int i=0; i<init; i++, num--) {
        res = 0;
        buffer = num;
        for (int j=0; j<num; j++ ) {
            res = num % buffer;
            buffer -= 1;
            if (res == 0) 
                printf ( "%u %% %u = 0\n", num, buffer+1);

        }
    }


    return EXIT_SUCCESS;
}
