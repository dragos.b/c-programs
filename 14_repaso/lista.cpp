
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 255
const char *palabras[] = {
    "Buenos Dias",
    "Buenas Noches",
    "Adios",
    "Tengo un poco de hambre",
    NULL
};
void banner ( const char *titulo ) {
    char comando[50] = "toilet -fpagga -F crop:border:gay -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

char frase ( char *phrase ) {
    fgets ( phrase, MAX, stdin );
    return *phrase;
}

int divide ( int dividendo, int divisor ){
    return dividendo/divisor;
}

int main (int argc, char *argv[]) {
    banner ( "Lista" );
    char phrase[MAX];
    long unsigned int total_p;

    printf ( "Introduce una frase\n" );
    frase ( phrase );
    printf ("Tu frase es:\n\t%s", phrase);

    printf ( "Palabra ocupa: %li\n", sizeof(palabras));

    total_p = divide (
            sizeof(palabras),
            sizeof(char*) );

    printf ( "Cantidad de celdas %lu\n\n", total_p);

    /*
    for (int i=0; i<total_p; i++)
        printf ( "%s\n", palabras[i] );
    */
    const char **p;
    p = palabras;
    while (*p != NULL) {
        printf ( "%s\n", *p);
        ++p;
    }
    return EXIT_SUCCESS;
}

