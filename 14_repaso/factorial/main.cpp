#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    double num, res=1;
    banner ( "Factorial" );

    // Entrada
    printf ( "Introduce un número:\n" );
    scanf ( " %lf", &num );
    int buffer = num;

    // Calculos
    for (int i=0; i<num; i++, buffer--)
        res *= buffer;
    buffer = num;

    // Salida
    printf ( "%.0lf! es:\n", num );
    for (int i=0; i<num; i++, buffer--) {
        printf ( "%i", buffer );
        if ( buffer > 1 ) {
            printf ( " x " );
        }
    }

    printf ( " = %.0lf\n", res );

    return EXIT_SUCCESS;
}
