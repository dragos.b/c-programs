#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

int
input_num (int num) {
    printf ( "Introduce Número:\n" );
    scanf ( " %i", &num );
    return num;
}

/**
 * @brief Multiplica dos numeros al estilo Russe
 *
 * @return int res El resultado de la multiplicación
 * @param x Entero al que se multiplica y
 * @param y Entero que multiplico con x
 * @var res Entero que guarda el resultado
 *
 */
int
russe ( int x, int y ) {
    int res=0;
    do {
        if ( (x%2) == 1 )     // Guardamos en res el numero de 'y' que se corresponde con 'x' y es impar.
            res += y;

        x >>= 1;              // Desplazamos 1 bit a la derecha en 'x', es decir, dividimos entre 2.
        y <<= 1;              // Desplazamos 1 bit a la izquierda en 'y', es decir, multiplicamos por 2.
    } while ( x>=1 );
    return res;
}

int
main (int argc, char *argv[]) {
    banner ( "Russe" );
    // Entrada
    int x=0, y=0, res;
    x = input_num(x);
    y = input_num(y);
    int xmod = x, ymod = y;

    // Calculos
    res = russe(x,y);

    // Salida
    printf ( "%i x %i = %i\n", xmod, ymod, res );

    return EXIT_SUCCESS;
}

