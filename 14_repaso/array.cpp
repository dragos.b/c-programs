#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX 0x14

void banner ( const char *titulo ) {
    char comando[50] = "toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( "clear" );
    system ( comando );
}

int main (int argc, char *argv[]) {
    // Entrada
    unsigned char l[MAX]; /* En cada celda guardo un caracter (una letra/numero) */
                          /* Si fuese con *l[] guardariamos en cada celda una palabra*/ 


    // Calculos
    for (int i=0; i<MAX; i++)
        l[i] = /*(unsigned char)*/ pow(i+1,2);

    // Salida
    banner ( "Array" );
    for (int i=0; i<MAX; i++)
        printf ( "&: %p - Celda : %02u número %02u = %02u\n",
                &l[i],
                i,
                i+1,
                l[i] );

    return EXIT_SUCCESS;
}
