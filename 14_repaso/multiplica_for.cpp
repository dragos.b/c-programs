#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "multiplica.h"
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

void input (int *num) {
    printf ( "Input number:\n" );
    scanf ( " %i", num );
}

int
main (int argc, char *argv[]) {
    banner ( "Multiplica" );
    int num1, num2;
    input (&num1);
    input (&num2);

    printf ( "%i X %i = %i\n", num1, num2, mult(&num1, &num2) );

    return EXIT_SUCCESS;
}
