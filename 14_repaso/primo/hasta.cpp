#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "calc.h"
#include "common.h"

/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    int tope, res, init;
    banner ( "Primos Hasta x" );

    printf ( "Averiguar primos hasta un tope\nIntroduce tope:\n" );
    scanf ( " %u", &tope );
    init = tope;
    printf ( "Primos hasta %i\n", init );
    primos_hasta (tope);

    return EXIT_SUCCESS;
}
