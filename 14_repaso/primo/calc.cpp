#include <stdio.h>
#include "calc.h"
#include "common.h"

int primo (int num) {
    int total[num-COMIENZO];
    // Guarda num % i en total[0]--> total[1]...
    for (int i=COMIENZO; i<=num; i++)
        total[i-COMIENZO] = num % i;

    // Compara si en cada posicion de total hay un 0, si hay un cero devuelve un 0 (Multiplo)
    // Si hay otra cosa continua el bucle hasta el final o hasta un 0
    // Si llega al final sin encontrar un 0 devuelve un 1 (Primo)
    for (int i=0;i<num-2;i++){
        if ( total[i] == 0 )
            return Multiplo;
        else continue;
    }
    return Primo;
}

void primos_tope (int tope) {
    int init = tope, res;
    for ( int i=1; i<init; i++, tope-- ) {
        res = primo(tope);
        if ( res == Primo )
            printf ( "%i ", tope );
    }
    printf ( "\n" );
}

void primos_hasta (int fin) {
    int init = fin, res;
    for (int i=init; i>=init; i--, fin--) {
        res = primo(fin);
        if ( res == Primo )
            printf ( "%i ", fin );
    }
    printf ( "\n" );
}
