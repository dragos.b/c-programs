#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "calc.h"

const char *num_type [] {
    "no es primo",
        "es primo"
};
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

int
main (int argc, char *argv[]) {
    int number, res;

    banner ( "Primo o No" );
    printf ( "¿ Es un numero primo ?\n\tIntroduce el número: \n" );
    scanf ( " %u", &number );

    res = primo (number);
    printf ( "%i %s\n", number, num_type[res] );

    return EXIT_SUCCESS;
}
