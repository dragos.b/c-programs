#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border:gay -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}
int
divisores ( int numero ) {                        // Devuelve la suma de los divisores de un numero
    int divs = 0;
    for ( int i=1; i <= (numero/2); i++)
        if ( numero % i == 0 )
            divs += i;
    return divs;
}
int
es_amigo ( int numero, int div ) {
    int two = div;
    int div_two = divisores(two);
    if ( numero != div_two && two != div )
        return 0;
    return 1;
}
int
main (int argc, char *argv[]) {
    banner ( "Amigos" );
    int one = 220,
        two = 0,
        div_one = 0,
        div_two = 0,
        res;

    // Calculos
    div_one = divisores ( one );

    two = div_one;
    div_two = divisores ( two );

    res = es_amigo ( one, div_one );
    res == 1 ? printf( "Son amigos %i - %i\n", one, two ) : printf ( "No son amigos\n" );

    return EXIT_SUCCESS;
}
