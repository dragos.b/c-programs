#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
void
banner ( const char *titulo ) {
    char comando[200] = "clear ; toilet -fpagga -F crop:border:gay -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}
int
main (int argc, char *argv[]) {
    banner ( "Matriz 2x4 4x3" );
    double frist [M][K] =
    {
        { 1, 2, 3, 4 },
        { 5, 6, 7, 8 }
    }, second [K][D] =
    {
        { 1, 2, 3 },
        { 4, 5, 6 },
        { 7, 8, 9 },
        { 2, 1, 4 }

    }, result [M][D];

    // Calculos
    for ( int i=0; i<M; i++ ) {
        for ( int j=0; j<D; j++ ) {
            for( int k=0; k<K; k++ )
                result[i][j] += frist[i][k] * second[k][j];
        }
    }

    // Salida
    for ( int i=0; i<M; i++ ) {
        printf( "\n" );
        for ( int j=0; j<D; j++ )
            printf ( "%.0lf ", result[i][j] );
    }
    printf( "\n" );

    return EXIT_SUCCESS;
}
