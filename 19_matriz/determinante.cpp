#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define D 3
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[200] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}
int
main (int argc, char *argv[]) {
    banner ( "Determinante" );
    int matriz [D][D];
    unsigned principal = 0,
             secundaria = 0,
             res_p,
             res_s,
             det;

    printf ( "Ingresa valores en la matriz\n" );
    for ( int i=0; i<D; i++ )
        for( int j=0; j<D; j++ )
            scanf ( " %i", &matriz[i][j] );

    // Calcula la principal
    for ( int i=0; i<D; i++ ) {
        res_p = 1;
        for ( int j=0; j<D; j++ )
            res_p *= matriz[(j+i) % D][j];
        principal += res_p;
    }

    // Calcula la secundaria
    for ( int i=3; i>0; i-- ) {
        res_s = 1;
        for ( int j=3, k=2; j<D*2; j++, k-- )
            res_s *= matriz[(j-i) % D][k];
        secundaria += res_s;
    }

    // Resultado de la determinante de la matriz
    det = principal - secundaria;

    // Salida
    for ( int i=0; i<D; i++ ) {
        printf ( "\n" );
        for( int j=0; j<D; j++ )
            printf ( "%i ", matriz[i][j] );
    }
    printf ( "\nEl resultado de la determinante es: %i\n", det );

    return EXIT_SUCCESS;
}
