#ifndef __CALC_H__
#define __CALC_H__

#ifdef __cplusplus
extern "C"
{
#endif

void defmatrix ( double **matrix, unsigned f, unsigned c );
void defresult ( double **matrix, unsigned f, unsigned c );
void calcmatrix ( double *first, double *second, double *result, unsigned m, unsigned k, unsigned n );

#ifdef __cplusplus
}
#endif

#endif
