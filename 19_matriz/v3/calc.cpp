#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "io.h"
#include "calc.h"

void
defmatrix ( double **matrix, unsigned f, unsigned c ) {                   // Introducir valores a una matriz en función del tamaño.
    banner ( "Dimension Desconocida" );
    *matrix = ( double * ) malloc ( f*c * sizeof(double) );
    printf ( "Introduce valores a la matriz:\n" );
    for ( int i=0; i<(f*c); i++ )
        scanf ( " %lf", *matrix+i );
}

void
defresult ( double **matrix, unsigned f, unsigned c ) {                   // Reseva de memoria para la matriz resultado.
    *matrix = ( double* ) malloc ( f*c * sizeof(double) );
}

void
calcmatrix ( double *first, double *second, double *result, unsigned m, unsigned k, unsigned n ) {
    bzero( result, m * n * sizeof(result) );                              // Se encuentra en strings.h, rellena todo de 0's.
    for ( int i=0; i<m; i++ )
        for ( int j=0; j<n; j++ ) {
            for ( int u=0; u<k; u++ )
                // result[i][j] += first[i][u] * second[u][j]
                // Se calcula el salto a mano, es decir el avance de n columnas por el salto, i
                *(result + i * n + j) += *(first + i * k + u) * *(second + u * n + j);
        }
}
