#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "io.h"

void
banner ( const char *titulo ) {
    char comando[250] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

void
ask_size ( unsigned *x, char letter ) {                                   // Pregunta tamaños
    banner ( "Dimension Desconocida" );
    printf ( "Tamaño para %c\n", letter );
    scanf ( " %u", x );
}

void
showmatrix ( double *matrix, unsigned f, unsigned c ) {                   // Printa matriz
    for ( int i=0, s=0; i<f; i++ ) {
        printf( "\n" );
        for ( int j=0; j<c; j++, s++ )
            printf ( "%3.0lf ", matrix[s] );
    }
    printf( "\n\n" );
}

void
pmname ( char name ) {
    printf ( "\tMatriz %c\n", name );
}
