#ifndef __IO_H__
#define __IO_H__

#ifdef __cplusplus
extern "C"
{
#endif

void banner ( const char *titulo );
void ask_size ( unsigned *x, char letter );
void showmatrix ( double *matriz, unsigned f, unsigned c );
void pmname ( char name );

#ifdef __cplusplus
}
#endif

#endif
