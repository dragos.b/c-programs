#include <stdio.h>
#include "common.h"
#include "io.h"
void
ingresa_m ( double matriz[D][D], int orden ) {
    printf ( "Introduce valores a la matriz %i:\n", orden );
    for ( int i=0; i<D; i++ )
        for( int j=0; j<D; j++ )
            scanf ( " %lf", &matriz[i][j] );
}
void
imprime_m ( double matriz[D][D], const char *titulo ) {
    printf ( "\t   %s\n", titulo );
    for ( int i=0;i<D;i++ ) {
        printf( "\n" );
        for ( int j=0; j<D; j++ )
            printf ( "\t%3.0lf ", matriz[i][j] );
    }
    printf( "\n\n" );
}
