#include <stdio.h>
#define MAX 10                            // Constant that define the maximum value of numbers to insert

//Calculate mean vlaue for MAX numbers

int main () {
  int num, count = 0;                     // num numbers input user, count counter to compare in the while 
  float m, result;                        // m = num, result = m/MAX
  
  printf("\nIntroduce un número\n");      // Input first number
  scanf("%d", &num);
 
  m = num;                                // Save the value of "num" in "m" to restore value to "num"
  num = 0;

  while ( count < MAX-1){
     printf("Introduce un número\n");
     scanf("%d", &num);                   // New value of "num"
     m = num+m;                           // New value of "m", adds the last value of "m" with new value of "num"
      count++;                            // Increment counter
  }

  result = m / MAX;                       // Calculate mean value & print the result
  printf("El resultado es: %f\n", result);
  return 0;

}
