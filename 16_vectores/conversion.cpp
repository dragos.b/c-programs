#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define PI 3.141592

const char *ejemplo[] = {
    "Cartesianas (2 3)",
    "Polares (36 5)",
    "Cilindricas (RO THETA Z)"
};
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

void options () {
    printf ( "\n\t1.- Cartesianas\n\t2.- Polares\n\t3.- Cilindricas\n");
}

void coor( double *ptr_c, unsigned int opc ) {
    double buffer;
    int count = 0;
    char end;
    printf ( "%s\n", ejemplo[opc-1] );
    scanf (" %*[()]");
    while ( !scanf( " %1[)]", &end ) ) {
        ptr_c = (double*) realloc(ptr_c, (count+1) * sizeof(double));
        scanf (" %lf", &buffer);
        ptr_c[count++] = buffer;
    }
}
void conv (double *ptr_c, unsigned int opc) {
    double radian, *res = NULL;
    switch (opc) {
        case 1:
            printf ("Cartesianas --> Polares");
            radian = atan2( ptr_c[1], ptr_c[0] );
            res = (double *) realloc(res, 2*sizeof(double));
            res[0] = radian / PI *180;
            res[1] = sqrt( pow(ptr_c[0],2) + pow(ptr_c[1],2) );
            printf("(%lf, %lf)", res[0], res[1]);
            break;
        case 2:
            break;
        case 3:
            break;
    }
}

int
main (int argc, char *argv[]) {
    banner ( "Conversion" );
    unsigned int opc;
    double ptr;

    printf ( "Convertir de " );
    options ();
    scanf ( " %i", &opc );
    switch (opc) {
        case 1:
            coor(&ptr, opc);
            conv(&ptr, opc);
            break;
        case 2:
            coor(&ptr, opc);
            break;

        case 3:
            coor(&ptr, opc);
            break;
    }
    //free(ptr);
    return EXIT_SUCCESS;
}
