#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "interfaz.h"
#include "calc.h"

const char *vectores[] = {
    "r ⃗",
    "s ⃗"
};
int
main (int argc, char *argv[]) {

    // Entrada
    banner ( "Angulo2Vec" );

    int vec_r[DIM], vec_s[DIM], resc;
    double rmod, rrad, rdeg;
    size ( vec_r );
    size ( vec_s );

    // Calculos
    resc = prodesc ( vec_r, vec_s );
    rmod = module ( vec_r, vec_s );
    rrad = degree ( resc, rmod );
    rdeg = rad2deg ( rrad );

    // Salida
    banner ( "Angulo2Vec" );
    printf ( "Producto escalar de los vectores:\n" );
    printf ( "%c", *vectores[0] );
    printv ( vec_r );
    printf ( "%c", *vectores[1] );
    printv ( vec_s );
    printf ( "\n     %i\n\n", resc );
    printf ( "Modulo: %.1lf\nGrados:(radianes)%.3lf\nGrados: %.2lfº\n", rmod, rrad, rdeg);

    return EXIT_SUCCESS;
}
