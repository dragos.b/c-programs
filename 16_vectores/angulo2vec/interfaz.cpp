#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "interfaz.h"

void banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 140 ";
    strcat ( comando, titulo );
    system ( comando );
}

void size (int vec[DIM]) {
    printf ( "Introduce valores al vector\n" );
    for (int i=0; i<DIM; i++)
        scanf ( " %i", &vec[i]);
}

void printv ( int vec[DIM] ) {
    printf ( "( ");
    for (int i=0; i<DIM; i++) {
        printf ( "%i ", vec[i] );
    }
    printf ( ")\n" );
}
