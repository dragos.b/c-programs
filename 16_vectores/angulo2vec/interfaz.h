#ifndef __INTERFAZ_H__
#define __INTERFAZ_H__

#include "common.h"

#ifdef __cplusplus
extern "C"
{
#endif
void banner ( const char *titulo );
void size ( int vec[DIM] );
void printv ( int vec_r[DIM] );
#ifdef __cplusplus
}
#endif

#endif
