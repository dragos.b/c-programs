#ifndef __CALC_H__
#define __CALC_H__

#include "common.h"

#ifdef __cplusplus
extern "C"
{
#endif

int prodesc ( int vec_r[DIM], int vec_s[DIM] );
double module ( int vec_r[DIM], int vec_s[DIM] );
double degree ( int prodesc, double mod );
double rad2deg ( double rad );

#ifdef __cplusplus
}
#endif

#endif
