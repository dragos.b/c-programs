#include "calc.h"
#include <math.h>

int prodesc (int vec_r[DIM], int vec_s[DIM]) {
    int res = 0;
    for (int i=0; i<DIM; i++)
        res += vec_r[i] * vec_s[i];
    return res;

    /**
     * R ( 1 2 1 )
     * S ( 1 1 1 )
     *
     * prodesc = R1 * S1 + R2 * S2 + R3 * S3
     *
     */
}

double module ( int vec_r[DIM], int vec_s [DIM] ) {
    double res_r = 0, res_s = 0, res = 0;
    for (int i=0; i<DIM; i++) {
        res_r += pow (vec_r[i], 2);
        res_s += pow (vec_s[i], 2);
    }
    res = res_r * res_s;
    return res;
    /**
     * R = 1^2 + 2^2 + 1^2 = sqrt(6)
     * S = 1^2 + 1^2 + 1^2 = sqrt(3)
     * modulo = R*S = sqrt(18)
     *
     */
}


double degree ( int prodesc, double mod ) { return acos ( prodesc / sqrt(mod) ); }
/**
 * grados = acos ( prodescalar / sqrt(modulo) )
 */

double rad2deg (double rad) { return ( rad * 180 ) / M_PI; }
