#include <math.h>
#include "fbd.h"

#define R 100


void circle () {
    for (double a=0; a<2*M_PI; a +=.001)
    put (R*cos(a), R*sin(a), 0xFF, 0xFF, 0xFF, 0xFF);
}

int main () {
    open_fb ();
    put ( 500, 350, 0xFF , 0xFF, 0xFF, 0xFF );
    circle ();
    close_fb ();

    return 0;  

}

