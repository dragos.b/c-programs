#include "fbd.h"
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>


int fb;
struct fb_var_screeninfo v_info;
struct fb_fix_screeninfo s_info;
char *buf;
size_t len;


void put ( int x, int y, char r, char g, char b, char a )
{
    uint32_t stride = s_info.line_length * y;
    x *= v_info.bits_per_pixel / 8;

    *(buf + stride + x     ) = b;
    *(buf + stride + x + 1 ) = g;
    *(buf + stride + x + 2 ) = r;
    *(buf + stride + x + 3 ) = a;
}

void info () {
    printf ("Length: %i bytes.\n", s_info.smem_len );
    printf (" %i x %i\n", v_info.xres, v_info.yres);
    printf (" %i \n", v_info.bits_per_pixel);
}

void open_fb () {
    fb = open ("/dev/fb0", O_RDWR);
    assert (fb > 0);

    if ( ioctl (fb, FBIOGET_FSCREENINFO, &s_info) < 0 ) {
        fprintf (stderr, "Couldn't fetch framebuffer screen info.\n");
        abort ();
    }

    if ( ioctl (fb, FBIOGET_VSCREENINFO, &v_info) < 0) {
        fprintf (stderr, "Couldn't fetch framebuffer variable info.\n");
        abort ();
    }
    len = s_info.smem_len;
    buf = (char *) mmap (NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, fb, 0);

    assert (buf != MAP_FAILED);

}

void close_fb () {
    munmap (buf, len);
    close (fb);

}
