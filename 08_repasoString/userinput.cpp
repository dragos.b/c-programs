#include <stdio.h>
#include <stdlib.h>

/**
 * Define constant '#' to print
 */
#define CARACTER 0x23

int main (int argc, char *argv[]) {
    int veces;
    /**
     * Ask the user the number of times to print CARACTER
     */
    printf("Número de veces para imprimir almoadilla: ");
    /**
     * Save the input value in position memory &veces
     */
    scanf(" %i", &veces);
    /**
     * While i<veces --> pirnt CARACTER and add 1 to i
     */ 
    for (int i=0; i<veces; i++)
        printf("%c", CARACTER);
    printf("\n");

    return EXIT_SUCCESS;
}
