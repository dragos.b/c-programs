#include <stdio.h>
#include <stdlib.h>

/**
 * Create constants suma, resta, multiplicacion, division; and their value start to
 * 0, 1, 2, 3
 */

enum Opcion { suma, resta, multiplicacion, division };
/**
 * Use the array of arrays to print the option chosen 
 */
const char *opciones[] = {
    "Suma",
    "Resta",
    "Multiplicación",
    "División", 
    NULL
};

int menu () {
    int opcion;
    system ("clear");
    system ("toilet -fpagga --metal Calculadora");

    printf (
            "\n\
            MENU\n\
            ****\n\
            \n\
            1.- Suma \n\
            2.- Resta \n\
            3.- Multiplicación \n\
            4.- División \n\
            \n\
            Introduce opción: \n\
            ");
    scanf (" %i", &opcion);
    return opcion - 1;
}

int main (int argc, char *argv[]) {
    int op1, op2;
    int calculo = menu ();

    printf ("Has elegido %s\n\n", opciones[calculo] );
    printf ("Introduce primer número " );
    scanf (" %i", &op1 );
    printf ("Introduce segundo número " );
    scanf (" %i", &op2 );

    int SResultado = op1 + op2;
    int RResultado = op1 - op2;
    int MResultado = op1 * op2;
    int DResultado = op1 / op2;

    switch ( calculo ) {
        case suma:
            printf ( "%i + %i = %i\n", op1, op2, SResultado );
            break;

        case resta:
            printf ( "%i - %i = %i\n", op1, op2, RResultado );
            break;
        case multiplicacion:
            printf ( "%i x %i = %i\n", op1, op2, MResultado );
            break;

        case division:
            printf ( "%i / %i = %i\n", op1, op2, DResultado );
            break;

        default:
            fprintf ( stderr, "Opción incorrecta \n" );
            return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
