#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    /**
     * Create variables 
     */
    int base,i,a;

    /**
     * Input base to print triangle 
     */
    printf("Introduce Base: ");
    scanf(" %i", &base);

    for (i = 0; i<=base; i++){
        for ( a = 0; a<i; a++ )
            printf("⏹ ");
        printf("\n");
    }    
    printf("\n");
    for (i = 0; i<=base; i++){
        for ( a = base; a>i; a-- )
            printf("⏹ ");
        printf("\n");
    }   


    printf("\n");
    for (i = 0; i<=base; i++){
        for ( a = base; a>=1; a-- )
            if ( a<=i )
                printf("⏹ ");
            else
                printf(" ");
        printf("\n");
    }    


    return EXIT_SUCCESS;
}
