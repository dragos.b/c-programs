#include <stdio.h>
#include <stdlib.h>

enum TPalo {oros, copas, espadas, bastos, PALOS};
const char *palos[] = {
    "Oros",
    "Copas",
    "Espadas",
    "Bastos",
    NULL
};


enum TPalo menu () {
    int opcion;
    for (int i=0; i<PALOS; i++)
        printf ("\t%i.- %s\n", i+1, palos[i]);
    printf ("\n");
    do {
        printf ("Opcion: ");
        scanf (" %i", &opcion);
    } while (opcion <1 || opcion > PALOS);
    opcion --;

    return (enum TPalo) opcion;
}

int main (int argc, char *argv[]) {
    enum TPalo opcion;

    opcion = menu ();

    switch (opcion) {
        case oros:
            printf ("Oros.\n");
            break;
        default:
            printf ("No oros\n");
    }

    printf ("Elegido: %s\n", palos[opcion]);

    return EXIT_SUCCESS;
}
