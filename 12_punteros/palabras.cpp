#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    banner ( "Palabras" );

    const char *palabras[] = {
        "Carlos",
        "Ovi",
        "Dragos"
    };
    printf("%c\n", *(*palabras)); // C
    printf("%c\n", *(*palabras)+1); // D (ascii)
    printf ("%s\n", *(palabras+1)); // Ovi
    printf ( "%c\n", *(*(palabras+1)+1) ); // v


    return EXIT_SUCCESS;
}
