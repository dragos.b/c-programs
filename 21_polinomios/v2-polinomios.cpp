#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
void
banner ( const char *titulo ) {
    char comando[200] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}
double
f ( double *pol, double li, double ls, double inc, unsigned long grado ) {
    double buffer;
    double inc2 = li;
    for ( inc2; inc2<ls; inc2+=inc ) {
        buffer=0;
        for ( int j=0; j<grado; j++ ) {
            buffer += pol[j] * pow ( inc2, j );
        }
        printf ( "%.2lf\n", buffer );
    }
}
int
main (int argc, char *argv[]) {
    banner ( "Polinomios Limites" );
    double pol[] = { -3, 0, 4, 3 };
    unsigned long grado = sizeof ( pol ) / sizeof( double );
    double li, ls, inc;

    printf("Limite Inferior:\n");
    scanf ("%lf", &li);

    printf("Limite Superior:\n");
    scanf ("%lf", &ls);

    printf("Incremento:\n");
    scanf ("%lf", &inc);

    f ( pol, li, ls, inc, grado );
    return EXIT_SUCCESS;
}
