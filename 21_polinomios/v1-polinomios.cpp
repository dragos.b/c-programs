#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void
banner ( const char *titulo ) {
    char comando[200] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

double
f ( double *pol, int grado, double x ) {
    double buffer = 0;
    for ( int i=0; i<(grado+1); i++ )
        buffer += pol[i] * pow ( x, i );
    return buffer;
}
int
main (int argc, char *argv[]) {
    banner ( "Polinomios" );
    double pol[] = { -3, 0, 4, 3 };
    double x;

    // Entrada
    printf ( "Valor a X:\n" );
    scanf ( "%lf", &x );

    // Calculos y Salida
    printf ( "El resultado es: %.1lf\n", f ( pol, 3, x ) );

    return EXIT_SUCCESS;
}
