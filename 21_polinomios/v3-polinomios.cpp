#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

int
ask_pol ( int **poly ) {
    char buff[] = "0";
    unsigned summit = 0;
    printf ( "\tPolinomio\n (3x² + 2x + 1) = 3 2 1 *\n\
            \r Finaliza con cualquier caracter\n" );
    // Guardamos numeros mientras lo que introduzca sean numeros.
    do {
        *poly = ( int* ) realloc ( *poly, (summit+1) * sizeof(int) );
        scanf ( " %c", buff );
        *( *poly + summit++ ) = atoi ( buff );
    } while ( (*buff >= 48) && (*buff <= 57) );

    // Elimina la ultima reserva, que es cualquier caracter distinto de 0-9.
    *poly = ( int* ) realloc ( *poly, (summit-1) * sizeof(int) );
    return summit - 1;
}

void
ask_limits ( double *li, double *ls, double *av) {

    printf ( "Limite Inferior:\n" );
    scanf ( "%lf", li );

    printf ( "Limite Superior:\n" );
    scanf ( "%lf", ls );

    printf ( "Avance:\n" );
    scanf ( "%lf", av );
}

double
calc_y ( int *poly, int size, double x ) {
    double total = 0;
    for ( int i=0; i<size; i++)
        // 3x² + 2x + 1
        // x = 3
        // 3*3²... 2*3¹... 1*3⁰
        total += *( poly + i ) * ( pow (x, (size - 1) - i ) );
    return total;
}

void
area ( double li, double ls, double av, int *poly, int size ) {
    int puntos = ( ls-li ) / av + 1;
    double total;
    printf ( "Limite Inf: %.1lf\nLimite Sup: %.1lf\nAvance: %.1lf\n",
            li,
            ls,
            av );

    for ( int i=0; i<puntos; i++, li += av) {
        total = calc_y ( poly, size, li );
        printf ( "%.1lf = %.2lf\n",li, total );
    }
}


int
main (int argc, char *argv[]) {
    banner ( "V3-POL" );

    int *poly = NULL,
        size,
        x,
        y;
    double li,
           ls,
           av;

    size = ask_pol ( &poly );
    ask_limits ( &li, &ls, &av );
    area ( li, ls, av, poly, size );

    return EXIT_SUCCESS;
}
