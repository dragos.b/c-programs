#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}

/**
 * @brief Pide un numero maximo al cual llegar en la serie de fibonacii
 *
 * @return total El numero maximo de la serie
 * @param total Entero que no tiene valor
 */
int
maximo (int total) {
    printf ( "Total Fibonacci\n" );
    scanf ( " %i", &total );
    printf ("\n");
    return total;
}

/**
 * @brief Calcula la serie Fibonacci con un total determinado
 * @details Cuando se llama a la función se pasa como parametros
 *          un puntero (o array) entero y el total.
 *          A continuación asigna a la posición 0 y 1 de 'serie' un
 *          1.
 *          Una vez asignado los dos numeros hace el calculo de la
 *          serie 'total' veces.
 *
 * @return void
 * @param serie Puntero entero
 * @param total Entero que ha retornado 'maximo'
 */
void
fib ( int *serie, int total ) {
    serie[0] = 1, serie[1] = 1;
    for ( int i=2; i<total; i++ )
        serie[i] = serie[i-1] + serie[i-2];

}

void
salida ( int *serie, int total) {
    for ( int i=0; i<total; i++ )
        printf ( "%02i.-%04i\n", i+1, serie[i] );
}

int
main (int argc, char *argv[]) {
    banner ( "Fibonacci" );

    // ENTRADA
    int total = maximo(total);
    /* Crea la variable entera 'total' que guardará el retorno de 'maximo' */
    int serie[ total ];
    /* Crea el array entero 'serie' con un tamaño del valor de 'total' */

    // CALCULOS
    fib (serie, total); /* Llamada a la función 'fib' */

    // SALIDA
    salida (serie,total);

    /*
     * Al crear las variables en 'main' son variables locales de 'main'
     * Por este motivo no tenemos que retornar nada en la funcion 'fib'
     * y podremos utilizar la variable 'serie' para otra función, en
     * este caso para 'salida()', con los calculos realizados.
     */

    return EXIT_SUCCESS;
}
