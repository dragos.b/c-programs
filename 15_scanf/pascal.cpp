#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * @brief Imprime un banner como titulo del programa
 * @details Se llama a la función y se pasa como parametro un/a char/frase
 *          se une al final del array 'comando' el parametro 'titulo', y se
 *          ejecuta el comando mediante la función 'system'
 *
 * @return Void
 * @param titulo Puntero constante de tipo char
 * @var comando Array char con el comando a ejecutar
 */
void
banner ( const char *titulo ) {
    char comando[] = "clear ; toilet -fpagga -F crop:border -w 130 ";
    strcat ( comando, titulo );
    system ( comando );
}


int
main (int argc, char *argv[]) {
    banner ( "Pascual" );

    unsigned int fila,col;
    printf ( "Introduce Filas: \n" );
    scanf ( " %i", &fila );
    printf ( "Introduce Columnas: \n" );
    scanf ( " %i", &col );

    unsigned int pila[fila][col];
    pila[0][0] = 1;

    for (int f=2; f<fila; f++)
        pila[f][0] = 1;
    for (int c=0; c<col; c++)
        for (int f=2; f<fila; f++)
            pila[f][c] = pila[f-1][1] + pila[f-1][1-1];

    for (int c=0; c<col; c++) {
        printf("    ");
        for (int f=0; f<fila; f++)
            printf ( "%i\n", pila[f][c] );
    }



   return EXIT_SUCCESS;



}
